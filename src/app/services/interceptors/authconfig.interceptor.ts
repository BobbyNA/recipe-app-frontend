import { AuthService } from './../auth.service';
import { Observable } from 'rxjs';
import { Injectable, Injector } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from "@angular/common/http";

@Injectable()

export class AuthInterceptor implements HttpInterceptor {
    constructor(private authService: AuthService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler) {
        const authToken = this.authService.getToken();
        console.log('authToken: ' + authToken);
        const authReq = req.clone({
            setHeaders: {
                Authorization: "token " + authToken
            }
        });
        return next.handle(authReq);
    }
}
