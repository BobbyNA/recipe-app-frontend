import { AuthService } from './../../services/auth.service';
import { RecipesService } from './../recipes.service';
import { Recipes } from './../recipes.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.css'],
})
export class RecipeComponent implements OnInit {
  recipes: Recipes[] = [];

  constructor(private recipeService: RecipesService, public authService: AuthService) {}

  ngOnInit(): void {
    this.getRecipe();
  }

  getRecipe(): void {
    this.recipeService
      .getRecipes()
      .subscribe((recipes) => (this.recipes = recipes));
  }

  add(name: string): void {
    name = name.trim();
    if (!name) {
      return;
    }
    this.recipeService.addRecipe({ name } as Recipes).subscribe((recipes) => {
      this.recipes.push(recipes);
    });
  }

  delete(recipe: Recipes): void {
    this.recipes = this.recipes.filter((h) => h !== recipe);
    this.recipeService.deleteRecipe(recipe.id).subscribe();
  }
}
