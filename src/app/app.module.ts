import { httpInterceptorProviders } from './services/interceptors/index';
import { MessageService } from './components/message.service';
import { AuthService } from './services/auth.service';
import { HttpErrorHandler } from './http-error-handler.service';
import { AuthInterceptor } from './services/interceptors/authconfig.interceptor';
import { HttpClientModule, HttpClientXsrfModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RecipeComponent } from './components/recipe/recipe.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './components/login/login.component';
import { CreateIngredientComponent } from './components/create-ingredient/create-ingredient.component';
import { SearchComponent } from './components/search/search.component';
import { ConfigComponent } from './components/config/config.component';
import { RequestCache, RequestCacheWithMap } from './request-cache.service';

@NgModule({
  declarations: [
    AppComponent,
    RecipeComponent,
    LoginComponent,
    CreateIngredientComponent,
    SearchComponent,
    ConfigComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'My-Xsrf-Cookie',
      headerName: 'My-Xsrf-Header',
    })

  ],
  providers: [
    AuthService,
    HttpErrorHandler,
    MessageService,
    { provide: RequestCache, useClass: RequestCacheWithMap },
    httpInterceptorProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
